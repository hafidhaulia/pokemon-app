import { useState } from "react";
import { GET_POKEMON, GET_POKEMON_LIST, graphqlClient } from "../graphql/query";
import { PokemonDetail, PokemonListResponse } from "./models/pokemon";

type FetchPokemonListParams = {
  limit: number;
  offset: number;
};
export const useFetchPokemonList = () => {
  const [response, setResponse] = useState<PokemonListResponse | null>(null);
  const [isFetching, setIsFetching] = useState<boolean>(false);
  const fetchPokemonListAPI = (params: FetchPokemonListParams) => {
    setIsFetching(true);
    setResponse(null);
    graphqlClient
      .query({
        query: GET_POKEMON_LIST,
        variables: params,
      })
      .then(({ data }) => {
        setResponse(data.pokemons);
      })
      .catch((error: Error) => {
        console.log(error.message);
      })
      .finally(() => {
        setIsFetching(false);
      });
  };

  return { response, isFetching, fetchPokemonListAPI };
};

type FetchPokemonDetailParams = {
  name: string;
};
export const useFetchPokemonDetail = () => {
  const [response, setResponse] = useState<PokemonDetail | null>(null);
  const [isFetching, setIsFetching] = useState<boolean>(false);
  const fetchPokemonDetailAPI = (params: FetchPokemonDetailParams) => {
    setIsFetching(true);
    setResponse(null);
    graphqlClient
      .query({
        query: GET_POKEMON,
        variables: params,
      })
      .then(({ data }) => {
        setResponse(data.pokemon);
      })
      .finally(() => {
        setIsFetching(false);
      });
  };

  return { response, isFetching, fetchPokemonDetailAPI };
};

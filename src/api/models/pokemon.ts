import * as ioTs from "io-ts";

export const PokemonCodec = ioTs.type({
  name: ioTs.string,
  image: ioTs.string,
});
export type Pokemon = ioTs.TypeOf<typeof PokemonCodec>;

export const PokemonListResponseCodec = ioTs.type({
  next: ioTs.union([ioTs.string, ioTs.null]),
  previous: ioTs.union([ioTs.string, ioTs.null]),
  results: ioTs.array(PokemonCodec),
});
export type PokemonListResponse = ioTs.TypeOf<typeof PokemonListResponseCodec>;

export const PokemonTypeCodec = ioTs.type({
  type: ioTs.type({
    name: ioTs.string,
  }),
});
export type PokemonType = ioTs.TypeOf<typeof PokemonTypeCodec>;

export const PokemonMoveCodec = ioTs.type({
  move: ioTs.type({
    name: ioTs.string,
  }),
});
export type PokemonMove = ioTs.TypeOf<typeof PokemonMoveCodec>;

export const PokemonDetailCodec = ioTs.type({
  name: ioTs.string,
  sprites: ioTs.type({
    front_default: ioTs.string,
  }),
  types: ioTs.array(PokemonTypeCodec),
  moves: ioTs.array(PokemonMoveCodec),
});
export type PokemonDetail = ioTs.TypeOf<typeof PokemonDetailCodec>;

export const MyPokemonCodec = ioTs.type({
  name: ioTs.string,
  image: ioTs.string,
  nickname: ioTs.string,
});
export type MyPokemon = ioTs.TypeOf<typeof MyPokemonCodec>;

export const MyPokemonListCodec = ioTs.array(MyPokemonCodec);
export type MyPokemonList = ioTs.TypeOf<typeof MyPokemonListCodec>;

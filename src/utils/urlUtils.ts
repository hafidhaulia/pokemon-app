import * as queryString from "query-string";

export const getPageFromUrl = (searchParams: string): number => {
  const searchQuery = queryString.parse(searchParams) || "";
  const pageNumber = searchQuery.page ? Number(searchQuery.page) : 1;
  return pageNumber;
};

export const countOffset = (page: number, limit: number): number => {
  if (page < 0) return 0;
  return (page - 1) * limit;
};

import { countOffset, getPageFromUrl } from "./urlUtils";

describe("test getPageFromUrl", () => {
  it("passing empty location search", () => {
    const page = getPageFromUrl("");
    expect(page).toBe(1);
  });

  it("passing page as first query in location search", () => {
    const page = getPageFromUrl("?page=2");
    expect(page).toBe(2);
  });

  it("passing page as second query in location search", () => {
    const page = getPageFromUrl("?query=test&page=3");
    expect(page).toBe(3);
  });
});

describe("test countOffset", () => {
  it("passing positive page number", () => {
    const offset = countOffset(1, 20);
    expect(offset).toBe(0);
  });

  it("passing negative page number", () => {
    const offset = countOffset(-1, 20);
    expect(offset).toBe(0);
  });
});

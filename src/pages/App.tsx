import { BrowserRouter, Route, Switch } from "react-router-dom";
import { TopNav } from "../components/TopNav/TopNav";
import { Stack } from "@chakra-ui/react";
import { lazy, Suspense } from "react";
import { Loading } from "../components/Loading/Loading";

const PokemonListPage = lazy(() => import("./PokemonListPage/PokemonList"));
const MyPokemonListPage = lazy(
  () => import("./MyPokemonListPage/MyPokemonList")
);
const PokemonDetailPage = lazy(
  () => import("./PokemonDetailPage/PokemonDetailPage")
);

export const App = () => {
  return (
    <Stack height="100vh">
      <Suspense fallback={<Loading />}>
        <BrowserRouter>
          <TopNav />
          <Switch>
            <Route exact path="/">
              <PokemonListPage />
            </Route>
            <Route exact path="/my-pokemon">
              <MyPokemonListPage />
            </Route>
            <Route exact path="/:name">
              <PokemonDetailPage />
            </Route>
          </Switch>
        </BrowserRouter>
      </Suspense>
    </Stack>
  );
};

export default App;

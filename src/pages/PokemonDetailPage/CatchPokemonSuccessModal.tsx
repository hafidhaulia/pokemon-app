import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Input,
  Text,
} from "@chakra-ui/react";
import React, { useState } from "react";
import { PokemonDetail } from "../../api/models/pokemon";
import { useMyPokemonContext } from "../../context/myPokemonContext";

type CatchPokemonSuccessModalProps = {
  pokemon: PokemonDetail;
  isOpen: boolean;
  onClose: (pokemonNickname: string) => void;
};
export const CatchPokemonSuccessModal = (
  props: CatchPokemonSuccessModalProps
) => {
  const { isOpen, onClose, pokemon } = props;
  const { state, dispatch } = useMyPokemonContext();
  const nicknameList = state.myPokemonList.map(
    (myPokemon) => myPokemon.nickname
  );
  const [nickname, setNickname] = useState<string>("");
  const isNicknameUsed = nicknameList.includes(nickname);

  const savePokemon = (pokemonNickname: string) => {
    const newPokemon = {
      name: pokemon.name,
      nickname,
      image: pokemon.sprites.front_default,
    };
    dispatch({ type: "catch", pokemon: newPokemon });
    setNickname("");
    onClose(pokemonNickname);
  };

  const onNicknameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setNickname(event.currentTarget.value);
  };

  return (
    <Modal isOpen={isOpen} onClose={() => {}} isCentered>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>You catch {pokemon.name}!</ModalHeader>
        <ModalBody>
          <Input
            placeholder="Pokemon nickname"
            value={nickname}
            onChange={onNicknameChange}
            isInvalid={isNicknameUsed}
          />
          {isNicknameUsed && (
            <Text color="crimson">Nickname already in use</Text>
          )}
        </ModalBody>
        <ModalFooter>
          <Button
            colorScheme="green"
            mr={3}
            onClick={() => savePokemon(nickname)}
            disabled={nickname.length === 0 || nicknameList.includes(nickname)}
          >
            Ok
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};

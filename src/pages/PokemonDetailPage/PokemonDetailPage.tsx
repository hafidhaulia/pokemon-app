import {
  Box,
  Stack,
  Image,
  Flex,
  Badge,
  Text,
  Button,
  Heading,
  Center,
  useToast,
} from "@chakra-ui/react";
import { useFetchPokemonDetail } from "../../api/pokemon";
import { useParams } from "react-router";
import { useEffect } from "react";
import { Loading } from "../../components/Loading/Loading";
import { PokemonDetail } from "../../api/models/pokemon";
import { useMyPokemonContext } from "../../context/myPokemonContext";
import { useState } from "react";
import { CatchPokemonSuccessModal } from "./CatchPokemonSuccessModal";

type UrlMatchParams = {
  name: string;
};

const PokemonDetailPage = () => {
  const { response, isFetching, fetchPokemonDetailAPI } =
    useFetchPokemonDetail();
  const { name } = useParams<UrlMatchParams>();
  const { state } = useMyPokemonContext();
  const toast = useToast();
  const [isSuccessCatchPokemon, setIsSuccessCatchPokemon] =
    useState<boolean>(false);
  const catchPokemonToastId = "catchPokemonToastId";

  const fetchPokemonDetail = (name: string) => {
    fetchPokemonDetailAPI({ name });
  };

  useEffect(() => {
    fetchPokemonDetail(name);
  }, [name]);

  const renderTotalOwnedPokemon = () => {
    if (!response) return null;
    const filteredMyPokemonList = state.myPokemonList.filter(
      (myPokemon) => myPokemon.name === response.name
    );
    return (
      <Heading size="md">You have: {filteredMyPokemonList.length}</Heading>
    );
  };

  const catchPokemon = () => {
    const isCatchingPokemonSuccess = Math.random() < 0.5;
    if (isCatchingPokemonSuccess) {
      setIsSuccessCatchPokemon(true);
      return;
    }
    toast({
      position: "bottom-right",
      status: "error",
      duration: 3000,
      title: "Pokemon flee...",
    });
  };

  const renderCatchButton = () => {
    return (
      <Button
        bg="green.400"
        color="white"
        onClick={() => {
          if (!toast.isActive(catchPokemonToastId)) {
            toast({
              id: catchPokemonToastId,
              position: "bottom-right",
              status: "info",
              duration: 3000,
              title: "Catching pokemon...",
              onCloseComplete: catchPokemon,
            });
          }
        }}
        disabled={toast.isActive(catchPokemonToastId)}
      >
        Catch
      </Button>
    );
  };

  const renderPokemonDetail = (pokemon: PokemonDetail) => {
    return (
      <Box borderWidth="1px" borderRadius="lg" spacing="2">
        <Flex
          alignItems={{ base: "center", sm: "flex-start" }}
          direction={{ base: "column", sm: "row" }}
        >
          <Image
            minW={{ base: "250px", md: "300px" }}
            src={pokemon.sprites.front_default}
            alt={pokemon.name}
          />
          <Stack mx={{ base: "4", sm: "0" }}>
            <Heading as="h5" size="md" mt="3">
              Name
            </Heading>
            <Text>{pokemon.name}</Text>

            <Heading as="h5" size="md" mt="3">
              Type
            </Heading>
            <Flex>
              {pokemon.types.map((type) => (
                <Badge
                  key={type.type.name}
                  borderRadius="full"
                  px="2"
                  colorScheme="blue"
                  mr="2"
                >
                  {type.type.name}
                </Badge>
              ))}
            </Flex>

            <Heading as="h5" size="md" mt="3">
              Moves
            </Heading>
            <Flex wrap="wrap">
              {pokemon.moves.map((type) => (
                <Badge
                  key={type.move.name}
                  borderRadius="full"
                  px="2"
                  colorScheme="blue"
                  mr="2"
                  mb="2"
                >
                  {type.move.name}
                </Badge>
              ))}
            </Flex>
          </Stack>
        </Flex>
      </Box>
    );
  };

  const onCloseCatchPokemonSuccessModal = (pokemonNickname: string) => {
    toast({
      position: "bottom-right",
      status: "success",
      duration: 3000,
      title: `${pokemonNickname} registered`,
    });
    setIsSuccessCatchPokemon(false);
  };

  return (
    <>
      {isFetching && (
        <Center height="100%">
          <Loading />
        </Center>
      )}
      {!isFetching && (
        <Stack p="3">
          {!response && "fetchError"}
          {response && (
            <>
              <Flex justifyContent="space-between" alignItems="center">
                {renderTotalOwnedPokemon()}
                {renderCatchButton()}
              </Flex>
              {renderPokemonDetail(response)}
              <CatchPokemonSuccessModal
                pokemon={response}
                isOpen={isSuccessCatchPokemon}
                onClose={onCloseCatchPokemonSuccessModal}
              />
            </>
          )}
        </Stack>
      )}
    </>
  );
};

export default PokemonDetailPage;

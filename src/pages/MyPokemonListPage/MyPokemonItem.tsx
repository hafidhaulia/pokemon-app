import { Stack, Image, Button, Flex, Box, Text } from "@chakra-ui/react";
import { MyPokemon } from "../../api/models/pokemon";
import { useMyPokemonContext } from "../../context/myPokemonContext";

type MyPokemonItemProps = {
  pokemon: MyPokemon;
  onReleasePokemon: () => void;
};
export const MyPokemonItem = (props: MyPokemonItemProps) => {
  const { dispatch } = useMyPokemonContext();
  const { pokemon, onReleasePokemon } = props;

  const releasePokemon = () => {
    onReleasePokemon();
    dispatch({ type: "release", pokemon });
  };

  return (
    <Stack borderWidth="1px" borderRadius="lg" spacing="2" alignItems="center">
      <Image
        src={pokemon.image}
        alt={pokemon.name}
        width="100px"
        height="100px"
      />
      <Flex px="2" justifyContent="space-between" width="100%">
        <Box mr="2">Name:</Box>
        <Text isTruncated>{pokemon.name}</Text>
      </Flex>
      <Flex px="2" justifyContent="space-between" width="100%">
        <Box mr="2">Nickname:</Box>
        <Text isTruncated>{pokemon.nickname}</Text>
      </Flex>
      <Button
        width="100%"
        bg="red.400"
        color="white"
        onClick={releasePokemon}
        borderTopRadius="none"
      >
        Release
      </Button>
    </Stack>
  );
};

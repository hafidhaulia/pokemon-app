import { Center, Grid, Link, Stack, Text, useToast } from "@chakra-ui/react";
import { MyPokemonItem } from "./MyPokemonItem";
import { useMyPokemonContext } from "../../context/myPokemonContext";
import { MyPokemon } from "../../api/models/pokemon";

const MyPokemonListPage = () => {
  const { state } = useMyPokemonContext();
  const toast = useToast();

  if (!state.myPokemonList.length) {
    return (
      <Center height="100%">
        <Stack>
          <Text>You don't have pokemon</Text>
          <Link href="/">
            <Center py="2" bg="blue.400" color="white" rounded="full">
              Catch now
            </Center>
          </Link>
        </Stack>
      </Center>
    );
  }

  const onReleasePokemon = (pokemon: MyPokemon) => {
    toast({
      position: "bottom-right",
      status: "success",
      duration: 3000,
      title: `${pokemon.nickname} released`,
    });
  };

  return (
    <Stack p="3" width="100%">
      <Grid
        templateColumns={{
          base: "repeat(2, minmax(0, 1fr))",
          md: "repeat(4, minmax(0, 1fr))",
          lg: "repeat(6, minmax(0, 1fr))",
        }}
        gap={5}
      >
        {state.myPokemonList.map((pokemon) => (
          <MyPokemonItem
            key={pokemon.name}
            pokemon={pokemon}
            onReleasePokemon={() => onReleasePokemon(pokemon)}
          />
        ))}
      </Grid>
    </Stack>
  );
};

export default MyPokemonListPage;

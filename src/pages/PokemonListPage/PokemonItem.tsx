import { Link, Image, Stack, Text } from "@chakra-ui/react";
import { Pokemon } from "../../api/models/pokemon";
import { useMyPokemonContext } from "../../context/myPokemonContext";

type PokemonItemProps = {
  pokemon: Pokemon;
};
export const PokemonItem = (props: PokemonItemProps) => {
  const { state } = useMyPokemonContext();
  const { pokemon } = props;
  const ownedPokemon = state.myPokemonList.reduce((total, myPokemon) => {
    if (myPokemon.name === pokemon.name) total++;
    return total;
  }, 0);

  return (
    <Link href={`/${pokemon.name}`}>
      <Stack
        borderWidth="1px"
        borderRadius="lg"
        spacing="2"
        pb="5"
        alignItems="center"
      >
        <Image
          src={pokemon.image}
          alt={pokemon.name}
          width="100px"
          height="100px"
        />
        <Text fontSize="lg" fontWeight="500">
          {pokemon.name}
        </Text>
        <Text>You have: {ownedPokemon}</Text>
      </Stack>
    </Link>
  );
};

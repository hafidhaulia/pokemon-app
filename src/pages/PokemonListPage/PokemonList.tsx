import { Alert, AlertIcon, Center, Grid, Stack } from "@chakra-ui/react";
import { useFetchPokemonList } from "../../api/pokemon";
import { useLocation } from "react-router";
import { countOffset, getPageFromUrl } from "../../utils/urlUtils";
import { useEffect } from "react";
import { Loading } from "../../components/Loading/Loading";
import { PokemonItem } from "./PokemonItem";
import { PokemonListResponse } from "../../api/models/pokemon";
import { Pagination } from "../../components/Pagination/Pagination";

const POKEMON_LIMIT = 30;

const PokemonList = () => {
  const { response, isFetching, fetchPokemonListAPI } = useFetchPokemonList();
  const location = useLocation();

  const fetchPokemonList = () => {
    const page = getPageFromUrl(location.search);
    fetchPokemonListAPI({
      limit: POKEMON_LIMIT,
      offset: countOffset(page, POKEMON_LIMIT),
    });
  };

  useEffect(() => {
    fetchPokemonList();
  }, []);

  useEffect(() => {
    fetchPokemonList();
  }, [location.search]);

  const renderPokemonList = (response: PokemonListResponse) => {
    return (
      <>
        <Grid
          templateColumns={{
            base: "repeat(2, 1fr)",
            md: "repeat(4, 1fr)",
            lg: "repeat(6, 1fr)",
          }}
          gap={5}
        >
          {response.results.map((pokemon) => (
            <PokemonItem key={pokemon.name} pokemon={pokemon} />
          ))}
        </Grid>
        <Pagination next={response.next} previous={response.previous} />
      </>
    );
  };

  return (
    <>
      {isFetching && (
        <Center height="100%">
          <Loading />
        </Center>
      )}
      {!isFetching && (
        <Stack p="3">
          {!response && (
            <Alert status="error">
              <AlertIcon />
              Something went wrong
            </Alert>
          )}
          {response && renderPokemonList(response)}
        </Stack>
      )}
    </>
  );
};

export default PokemonList;

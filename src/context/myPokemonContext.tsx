import * as E from "fp-ts/lib/Either";
import * as J from "fp-ts/lib/Json";
import { pipe } from "fp-ts/lib/function";
import * as React from "react";
import {
  MyPokemon,
  MyPokemonList,
  MyPokemonListCodec,
} from "../api/models/pokemon";

const MY_POKEMON_LOCAL_STORAGE_KEY = "my-pokemon";

const getMyPokemonFromLocalStorage = (): MyPokemonList => {
  const myPokemonListString =
    localStorage.getItem(MY_POKEMON_LOCAL_STORAGE_KEY) || "";

  if (!myPokemonListString) return [];

  const parsedMyPokemonList = pipe(
    J.parse(myPokemonListString),
    E.fold(() => {
      return E.left(new Error("Failed parse json"));
    }, (json: J.Json) => {
      const decodeResult = MyPokemonListCodec.decode(json)
      if (E.isRight(decodeResult)) return E.right(decodeResult.right);
      return E.left(new Error("Failed decode json"));
    })
  )

  if (E.isRight(parsedMyPokemonList)) {
    return parsedMyPokemonList.right;
  }

  console.error(parsedMyPokemonList.left);
  return [];
};

const setMyPokemonToLocalStorage = (myPokemonList: MyPokemonList) => {
  localStorage.setItem(
    MY_POKEMON_LOCAL_STORAGE_KEY,
    JSON.stringify(myPokemonList)
  );
};

type Action = { type: "catch" | "release"; pokemon: MyPokemon };
type Dispatch = (action: Action) => void;
type State = {
  myPokemonList: MyPokemon[];
};

type MyPokemonContextProps = { state: State; dispatch: Dispatch };
const MyPokemonContext = React.createContext<MyPokemonContextProps | null>(
  null
);

export const myPokemonReducer = (state: State, action: Action) => {
  const { type, pokemon } = action;
  switch (type) {
    case "catch": {
      const newPokemonList = [...state.myPokemonList, pokemon];
      setMyPokemonToLocalStorage(newPokemonList);
      return { myPokemonList: newPokemonList };
    }
    case "release": {
      const filteredPokemonList = state.myPokemonList.filter(
        (myPokemon) => myPokemon.nickname !== pokemon.nickname
      );
      setMyPokemonToLocalStorage(filteredPokemonList);
      return { myPokemonList: filteredPokemonList };
    }
    default: {
      throw new Error(`Unhandled action type`);
    }
  }
};

export const MyPokemonProvider = ({
  children,
}: {
  children: React.ReactNode;
}) => {
  const [state, dispatch] = React.useReducer(myPokemonReducer, {
    myPokemonList: getMyPokemonFromLocalStorage(),
  });
  const value = { state, dispatch };
  return (
    <MyPokemonContext.Provider value={value}>
      {children}
    </MyPokemonContext.Provider>
  );
};

export const useMyPokemonContext = (): MyPokemonContextProps => {
  const context = React.useContext(MyPokemonContext);
  if (!context) {
    throw new Error("useMyPokemonContext must be used within a MyPokemonProvider");
  }
  return context;
};

import { Button, Flex, Text } from "@chakra-ui/react";
import { useHistory, useLocation } from "react-router";
import { getPageFromUrl } from "../../utils/urlUtils";
import * as querystring from "query-string";

type PaginationProps = {
  next: string | null;
  previous: string | null;
};
export const Pagination = (props: PaginationProps) => {
  const { next, previous } = props;
  const location = useLocation();
  const history = useHistory();
  const page = getPageFromUrl(location.search);

  const redirectPage = (page: number) => {
    const query = querystring.parse(location.search);
    query.page = String(page);
    history.replace({
      pathname: location.pathname,
      search: querystring.stringify(query),
    });
  };

  return (
    <Flex alignItems="center">
      <Button
        mr="3"
        isDisabled={!previous}
        onClick={() => redirectPage(page - 1)}
      >
        Prev
      </Button>
      <Text fontSize="md" mr="3">
        {page}
      </Text>
      <Button mr="3" isDisabled={!next} onClick={() => redirectPage(page + 1)}>
        Next
      </Button>
    </Flex>
  );
};

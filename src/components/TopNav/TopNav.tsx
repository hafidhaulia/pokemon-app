import {
  Flex,
  Heading,
  Link,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  Box,
} from "@chakra-ui/react";
import { useHistory } from "react-router";

export const DesktopMenu = () => {
  return (
    <Flex alignItems="center" display={{ base: "none", md: "initial" }}>
      <Link href="/" mr="4">
        Pokemon list
      </Link>
      <Link href="/my-pokemon">My Pokemon</Link>
    </Flex>
  );
};

const MobileMenu = () => {
  const history = useHistory();
  const goTo = (path: string) => history.push(path);

  return (
    <Flex display={{ sm: "initial", md: "none" }}>
      <Menu>
        <MenuButton size="sm">Menu</MenuButton>
        <MenuList fontSize="sm" color="black">
          <MenuItem onClick={() => goTo("/")}>Pokemon list</MenuItem>
          <MenuItem onClick={() => goTo("/my-pokemon")}>My pokemon</MenuItem>
        </MenuList>
      </Menu>
    </Flex>
  );
};

export const TopNav = () => {
  return (
    <Box
      bg="gray.700"
      w="100%"
      color="white"
      p="3"
      position="sticky"
      top={0}
      zIndex="sticky"
    >
      <Flex alignItems="center" justifyContent="space-between">
        <Heading size="md">Pokemon</Heading>
        <DesktopMenu />
        <MobileMenu />
      </Flex>
    </Box>
  );
};

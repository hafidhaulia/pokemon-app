# Pokemon App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Homepage
[https://pokemon-app-git-master-hafidhwirandi-gmailcom.vercel.app/](https://pokemon-app-git-master-hafidhwirandi-gmailcom.vercel.app/)

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

## Libraries & Tools
- [Apollo graphql client](https://www.apollographql.com/docs/react/)
- [Jest](https://jestjs.io/)
- [Chakra UI](https://chakra-ui.com/)
- [React router](https://reactrouter.com/web)
- [io-ts](https://gcanti.github.io/io-ts/)
- [query-string](https://github.com/sindresorhus/query-string)
- [Typescript](https://www.typescriptlang.org/)

## Testing
- [Prettier](https://prettier.io/)